from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    return jsonify({'books':books}),201

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == "POST":
        fname = request.form.get("fname")
        dct = {'title': fname, "id":(len(books)+1)}
        books.append(dct)
        return render_template('showBook.html', books=books)
    return render_template("newBook.html")

@app.route('/book/<int:book_id>/edit/' ,methods=['GET','POST'])
def editBook(book_id):
    if request.method == "POST":
        fname = request.form['fname'] 
        books[(book_id)-1]['title']=fname
        return render_template("showBook.html",books = books) 
    title = books[(book_id)-1]['title']
    return render_template("editBook.html", books = books, book_id = book_id, fname = books[book_id-1]["title"])
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        books.remove(books[book_id-1])
        for i in books:
            i['id'] = books.index(i) + 1
        return redirect(url_for('showBook', books = books))
    else:
        return render_template('deleteBook.html', books = books, book_id = book_id, fname = books[book_id-1]['title'])

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

